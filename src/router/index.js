import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    beforeEnter(to,from,next){
      if(window.isLg){
        next()
      }else{
        next({name:"Login"})
      }
    }
  },
  {
    path:'/storg',
    name:"Storg",
    component:() => import('../views/localStorg.vue'),
    children:[
      {
        path:'/upload',
        name:'Upload',
        component:()=>import('../views/upload1/upload.vue')
      }
    ]
  },
  {
    path:'/login',
    name:'Login',
    component:()=> import('../views/login/login.vue')
  }, {
    path:"newsList",
    name:"NewsList",
    component:()=>import('../views/newsList/NewsList.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
