import Vue from 'vue'
import Vuex from 'vuex'
import state from './state' // 存储变量
import mutations from './mutations' // 提交更新数据 
Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  state,
  mutations,
})




// 　　dispatch：异步操作，写法： this.$store.dispatch('mutations方法名',值)

// 　　commit：同步操作，写法：this.$store.commit('mutations方法名',值)