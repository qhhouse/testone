const path = require('path')
module.exports = {
  lintOnSave: false,

  configureWebpack:{
    devtool:"source-map"
  },
  devServer:{
    open: true,
    proxy:{
      "/api": {
        target: "https://unidemo.dcloud.net.cn",// 代理服务器地址
        ws: false,
        ChangeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      },
    }
  }
}
